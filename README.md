# Collab RAT - The cross-platform collaborative remote access software for collab-vm
Note: This is written as a header only library with one main class. This allows for adding this to your existing programs.

## Building from Linux to Windows
First run
```
git clone --recursive https://gitlab.com/ctrlaltf2/collab-rat
```
Then run (once)
```
cmake -DCMAKE_TOOLCHAIN_FILE=toolchain/Linux-{arch}-w64-mingw-7.2.0.cmake -DCMAKE_BUILD_TYPE={type} .
```
where {arch} is either `i686` or `x86_64` and `{type}` is either `DEBUG` or `RELEASE`.

Note: To switch between arch targets, you have to delete CMakeCache.txt.

Finally, run
```
make
```
and you should have the executable in bin/

## Building from Linux to Linux
Soon™ (Just some CMake cancer needed and it can be done)

## Currently implemented commands
- echo: mostly for debug, sends back what you send it
- exit: ~~gracefully~~ kills the server
- exec: Basically a wrapper for system() so any command you type in will be run like that

Adding your own commands is *extremely* easy. This is by design to allow for easily extending this in your own programs if you wanted to.

## Contributing
Want to add a command that executes some crazy payload that you wrote in one of you own ~~viruses~~ programs? Add it to `src/Main.cpp` and make a pull request!

## Todo
- Some sort of authentication system
- A web client
- Building on Windows hosts
- Building for Linux on Linux
