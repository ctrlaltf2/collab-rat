#include <iostream>
#include <windows.h>

// If the VMs snapshots ever change or the ports change, or whatever, get the disk serial number easily with this program

int main()  {
    char lpVolumeNameBuffer[64], lpFileSystemNameBuffer[64];
    DWORD nVolumeNameSize = 64, nFileSystemNameSize = 64, lpVolumeSerialNumber, lpMaximumComponentLength, lpFileSystemFlags;
    GetVolumeInformation("C:\\", 
            lpVolumeNameBuffer,
            nVolumeNameSize,
            &lpVolumeSerialNumber,
            &lpMaximumComponentLength,
            &lpFileSystemFlags,
            lpFileSystemNameBuffer,
            nFileSystemNameSize
    );

    std::cout << std::string(lpVolumeNameBuffer) << std::endl;
    std::cout << lpVolumeSerialNumber << std::endl;

}
