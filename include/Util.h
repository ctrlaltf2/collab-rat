#pragma once
#include <string>

#include <websocketpp/common/connection_hdl.hpp>

struct Command {
    std::string name;
    std::string params;
    websocketpp::connection_hdl hdl;
};

